<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmOrdersRetalix extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_orders_retalix';

    protected $fillable = [
        'id',
        'store_id',
        'pump_number',
        'product_price',
        'payment_type',
        'status',
        'create_date',
        'modify_date',
        'system_type',
        'nipl_order_id',
        'total_order_amt',
        'nipl_order_id'
    ];
}
