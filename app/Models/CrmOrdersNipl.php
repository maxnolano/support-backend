<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmOrdersNipl extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_orders_nipl';

    protected $fillable = [
        'id',
        'store_id',
        'pump_number',
        'product_price',
        'order_payment_type',
        'status',
        'created_at',
        'updated_at',
        'product_id',
        'total_order_amt',
        'external_id'
    ];
}