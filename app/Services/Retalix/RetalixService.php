<?php

namespace App\Services\Retalix;

use App\Repositories\Retalix\RetalixRepository;
use App\Repositories\Interfaces\RetalixRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmOrdersRetalix as RetalixResource;
use App\Http\Resources\CrmOrdersRetalixCancelledCompleted as RetalixCancelledResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for getting datas from orders_retalix table
 */
class RetalixService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for retalix orders
    private RetalixRepositoryInterface $retalixRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // RetalixRepository initialization
        $this->retalixRepository = new RetalixRepository();

        $this->client = new Client();
    }

    /**
     * List of all orders
     * @param int $system_type
     * @return Object
     */
    public function orders(int $system_type): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['store_id'] = $this->request->store_id;
            $request_params['st_title'] = $this->request->st_title;
            $request_params['pump_number'] = $this->request->pump_number;
            $request_params['product_price'] = $this->request->product_price;
            $request_params['payment_type'] = $this->request->payment_type;
            $request_params['status'] = $this->request->status;
            $request_params['create_date'] = $this->request->create_date;
            $request_params['modify_date'] = $this->request->modify_date;
            $request_params['nipl_order_id'] = $this->request->nipl_order_id;
            $request_params['total_order_amt'] = $this->request->total_order_amt;
            $request_params['external_id'] = $this->request->external_id;

            $orders = $this->retalixRepository->orders($request_params, $system_type);
            return $this->result(RetalixResource::collection($orders));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Exact order
     * @param int $system_type
     * @return Object
     */
    public function order(int $system_type): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request->id;

            $order = $this->retalixRepository->order($request_params, $system_type);

            if(count($order) == 0){
                return $this->result(['data' => [['c_id' => 'Данные для этого заказа отсутсвуют']]]); 
            }else{
                return $this->result(['data' => $order]);
            }
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update order
     * @return Object
     */
    public function update(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['value'] = $this->request->value;

            $updates = $this->retalixRepository->update($request_params);
            return $this->result(['data' => [$updates['retalix_status'], $updates['retalix_cancelled_status']]]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update status (canceling)
     * @return Object
     */
    public function cancel(): array
    {
        try{
            $result = [];

            $url =  config('app.base_retalix_url')  . 'extapi/order/cancel';
            $fields = json_encode(['order_id' => $this->request->order_id]);

            $request = new ClientRequest('POST', $url);

            $response =  $this->client->send($request, [
                'body' => $fields,
                'headers' => [
                    'Content-Type' => 'Content-Type: application/json',
                    'Authorization' => 'a7e1cb32-746a-44b8-8d0c-ea24ee05e134'
                ],
                'connect_timeout' => 20,
                'verify' => false,
                'http_errors' => false,
            ]);

            if ($response) {
                $responseText = $response->getBody()->getContents();
                $responseCode = $response->getStatusCode();
                $result['content'] = $responseText;
                $result['code'] = $responseCode;
                $result['type'] = 'cancel';

                $request_params = [];
                $request_params['id'] = $this->request->item_id;
                $request_params['value'] = 5;

                $updates = $this->retalixRepository->update($request_params);
                $result['retalix_status'] = $updates['retalix_status'];
                $result['retalix_cancelled_status'] = $updates['retalix_cancelled_status'];
            } else {
                $result['code'] = '500';
                $result['type'] = 'cancel';
            }

            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update status (refunding)
     * @return Object
     */
    public function refund(): array
    {
        try{
            $result = [];

            $url = config('app.base_retalix_url') . 'extapi/order/refund';
            $fields = json_encode([
                'order_id' => $this->request->order_id, 
                'actual_filled_liters' => $this->request->actual_filled_liters, 
                'store_product_id' => $this->request->store_product_id
            ]);

            $request = new ClientRequest('POST', $url);

            $response =  $this->client->send($request, [
                'body' => $fields,
                'headers' => [
                    'Content-Type' => 'Content-Type: application/json',
                    'Authorization' => 'a7e1cb32-746a-44b8-8d0c-ea24ee05e134'
                ],
                'connect_timeout' => 20,
                'verify' => false,
                'http_errors' => false,
            ]);

            if ($response) {
                $responseText = $response->getBody()->getContents();
                $responseCode = $response->getStatusCode();
                $result['content'] = $responseText;
                $result['code'] = $responseCode;
                $result['type'] = 'refund';

                $request_params = [];
                $request_params['id'] = $this->request->item_id;
                $request_params['value'] = 5;

                $updates = $this->retalixRepository->update($request_params);
                $result['retalix_status'] = $updates['retalix_status'];
                $result['retalix_cancelled_status'] = $updates['retalix_cancelled_status'];
            } else {
                $result['code'] = '500';
                $result['type'] = 'refund';
            }

            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
