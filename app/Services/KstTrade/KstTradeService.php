<?php

namespace App\Services\KstTrade;

use App\Repositories\KstTrade\KstTradeRepository;
use App\Repositories\Interfaces\KstTradeRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmOrdersNipl as KstTradeResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for getting datas from orders_nipl table
 */
class KstTradeService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for KstTrade orders
    private KstTradeRepositoryInterface $kstTradeRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // KstTradeRepository initialization
        $this->kstTradeRepository = new KstTradeRepository();

        $this->client = new Client();
    }

    /**
     * List of all orders
     * @return Object
     */
    public function orders(): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['store_id'] = $this->request->store_id;
            $request_params['st_title'] = $this->request->st_title;
            $request_params['pump_number'] = $this->request->pump_number;
            $request_params['product_price'] = $this->request->product_price;
            $request_params['order_payment_type'] = $this->request->order_payment_type;
            $request_params['status'] = $this->request->status;
            $request_params['created_at'] = $this->request->created_at;
            $request_params['updated_at'] = $this->request->updated_at;
            $request_params['total_order_amt'] = $this->request->total_order_amt;
            $request_params['external_id'] = $this->request->external_id;

            $orders = $this->kstTradeRepository->orders($request_params);
            return $this->result(KstTradeResource::collection($orders));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Exact order
     * @return Object
     */
    public function order(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request->id;

            $order = $this->kstTradeRepository->order($request_params);

            if(count($order) == 0){
                return $this->result(['data' => [['c_id' => 'Данные для этого заказа отсутсвуют']]]); 
            }else{
                return $this->result(['data' => $order]);
            }
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update order
     * @return Object
     */
    public function update(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['value'] = $this->request->value;

            $updates = $this->kstTradeRepository->update($request_params);
            return $this->result(['data' => [$updates['kstTrade_status'], $updates['kstTrade_cancelled_status']]]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update status (canceling)
     * @return Object
     */
    public function cancel(): array
    {
        try{
            $result = [];
            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update status (refunding)
     * @return Object
     */
    public function refund(): array
    {
        try{
            $result = [];
            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
