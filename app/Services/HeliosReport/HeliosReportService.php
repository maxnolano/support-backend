<?php

namespace App\Services\HeliosReport;

use App\Repositories\HeliosReport\HeliosReportRepository;
use App\Repositories\Interfaces\HeliosReportRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use Mail;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Exports\HeliosReportExport;

/**
 * Service for getting KstTrade export
 */
class HeliosReportService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for KstTrade orders
    private HeliosReportRepositoryInterface $HeliosReportRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // HeliosReportRepository initialization
        $this->HeliosReportRepository = new HeliosReportRepository();

        $this->client = new Client();
    }

    /**
     * Get the excel report
     * @return Object
     */
    public function report(): Object
    {
        try {
            $request_params = [];
            $request_params['city'] = $this->request->city;
            $request_params['stores'] = $this->request->stores;
            $request_params['agents'] = $this->request->agents;
            $request_params['start'] = $this->request->start;
            $request_params['end'] = $this->request->end;

            $start = join("", explode("-", explode(" ", $request_params['start'])[0]));

            $end = join("", explode("-", explode(" ", $request_params['end'])[0]));

            // $city = strtolower($request_params['city']);
            $city = 'chosen-cities';

            $agent = '';

            if(count($request_params['agents']) < 2){
                $agent = $request_params['agents'][0];

                switch ($request_params['agents'][0]) {
                    case '9':
                        $agent = 'kaspi';
                        break;
                    case '8':
                        $agent = 'halyk';
                        break;
                    case '10':
                        $agent = 'alacard';
                        break;
                    default:
                        $agent = 'smartzapravka';
                }
            }

            $filename = $agent ? 'report_'.$city.'_'.$agent.'_'.$start.'_'.$end.'.xlsx' : 'report_'.$city.'_'.$start.'_'.$end.'.xlsx';

            Excel::store(new HeliosReportExport($request_params), $filename, 'public_uploads', null, [
                'visibility' => 'public',
            ]);
            
            $fullPath = Storage::disk('public_uploads')->path($filename);

            return response()->json([
                'data' => $filename,
                'message' => 'Helios report is successfully exported.'
            ], 200);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Send excel report via mail
     * @return Array
     */
    public function sendReport(): Array
    {
        try {
            $data['mail'] = $this->request->mail;
            $data['filename'] = $this->request->filename;
            
            $file = public_path('reports/'.$data['filename']);
    
            Mail::send('mail.mail-pattern', $data, function($message)use($data, $file) {
                $message->to($data['mail'])
                        ->subject('Отчет по Helios от Compass');
    
                $message->attach($file);
            });

            return ['add' => $this->request->mail, 'file' => $this->request->filename];
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get cities for this endpoint
     * @return Array
     */
    public function cities(): Array
    {
        try {
            $result = $this->HeliosReportRepository->cities();

            return $this->result(['data' => $result]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get stores for this endpoint
     * @return Array
     */
    public function stores(): Array
    {
        try {
            $request_params = [];
            $request_params['city'] = $this->request->city;

            $result = $this->HeliosReportRepository->stores($request_params);

            return $this->result(['data' => $result]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
