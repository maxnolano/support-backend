<?php

namespace App\Services\Individual;

use App\Repositories\Individual\IndividualRepository;
use App\Repositories\Interfaces\IndividualRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use Illuminate\Http\Response;
use Mail;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Exports\ExcelExport;

/**
 * Service for getting datas from orders_retalix table
 */
class IndividualService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for retalix orders
    private IndividualRepositoryInterface $individualRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // RetalixRepository initialization
        $this->individualRepository = new IndividualRepository();

        $this->client = new Client();
    }

    /**
     * Get the excel report
     * @return Object
     */
    public function report(): Object
    {
        try {
            $request_params = [];
            $request_params['city'] = $this->request->city;
            $request_params['stores'] = $this->request->stores;
            $request_params['agents'] = $this->request->agents;
            $request_params['start'] = $this->request->start;
            $request_params['end'] = $this->request->end;
            $request_params['columnAgent'] = $this->request->columnAgent;

            $start = join("", explode("-", explode(" ", $request_params['start'])[0]));

            $end = join("", explode("-", explode(" ", $request_params['end'])[0]));

            $city = 'chosen-cities';

            $agent = '';

            if(count($request_params['agents']) < 2){
                $agent = $request_params['agents'][0];

                switch ($request_params['agents'][0]) {
                    case '9':
                        $agent = 'kaspi';
                        break;
                    case '8':
                        $agent = 'halyk';
                        break;
                    case '10':
                        $agent = 'alacard';
                        break;
                    default:
                        $agent = 'smartzapravka';
                }
            }

            $reports = [];
            if ($this->request['columnUnion'] == 'true') {
                $filename = $agent ? 'report_'.$city.'_all_'.$agent.'_'.$start.'_'.$end.'.xlsx' : 'report_'.$city.'_all_'.$start.'_'.$end.'.xlsx';

                Excel::store(new ExcelExport($request_params), $filename, 'public_uploads', null, [
                    'visibility' => 'public',
                ]);

                $reports[] = [
                    'filename' => $filename,
                    'path' => 'public_uploads/' . $filename,
                ];
            } else {
                foreach ($request_params['stores'] as $store) {
                    $r = array();
                    $r[] = $store;
                    $request_params['stores'] = $r;

                    $filename = $agent ? 'report_' . $city . '_' . $store . '_' . $agent . '_' . $start . '_' . $end . '.xlsx' : 'report_' . $city . '_' . $store . '_' . $start . '_' . $end . '.xlsx';

                    Excel::store(new ExcelExport($request_params), $filename, 'public_uploads', null, [
                        'visibility' => 'public',
                    ]);

                    $reports[] = [
                        'filename' => $filename,
                        'path' => 'public_uploads/' . $filename,
                    ];
                }
            }
            return response()->json([
                'data' => $reports,
                'message' => 'Individuals report is successfully exported.'
            ]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Send excel report via mail
     * @return Array
     */
    public function sendReport(): Array
    {
        try {
            $data['mail'] = $this->request->mail;
            $data['filename'] = $this->request->filename;

            $file = public_path('reports/'.$data['filename']);

            Mail::send('mail.mail-pattern', $data, function($message)use($data, $file) {
                $message->to($data['mail'])
                        ->subject('Отчет от Compass');

                $message->attach($file);
            });

            return ['add' => $this->request->mail, 'file' => $this->request->filename];
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get cities for this endpoint
     * @return Array
     */
    public function cities(): Array
    {
        try {
            $result = $this->individualRepository->cities();

            return $this->result(['data' => $result]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get stores for this endpoint
     * @return Array
     */
    public function stores(): Array
    {
        try {
            $request_params = [];
            $request_params['city'] = $this->request->city;

            $result = $this->individualRepository->stores($request_params);

            return $this->result(['data' => $result]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
