<?php

namespace App\Services\Individual;

use App\Services\BaseService;
use Illuminate\Http\Request;
use Mail;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Exports\IndividualCronExport;
use Illuminate\Support\Facades\DB;

/**
 * Individual cron jobs
 */
class IndividualCronService extends BaseService
{
    // Incoming request
    private Array $request;

    /**
     * Class constructor
     * @param Array $request
     */
    public function __construct(Array $request)
    {
        // Request initialization
        $this->request = $request;
    }

    /**
     * Get the excel report
     * @return Object
     */
    public function report(): Object
    {
        try {
            $request_params = [];
            $request_params['start'] = $this->request['start'];
            $request_params['end'] = $this->request['end'];
            $request_params['agents'] = [];

            $cities = $this->cities();

            foreach($cities as $c){
                $request_params['city_object'] = ['city' => $c->city];

                $stores = $this->stores($request_params['city_object']);
                $store_ids = [];
                foreach($stores as $s){
                    $store_ids[] = $s->id;
                }

                $request_params['stores'] = $store_ids;

                $request_params['agents'][] = 9;
                $request_params['agents'][] = 8;
                $request_params['agents'][] = 1;
                $request_params['agents'][] = 10;

                $start = join("", explode("-", explode(" ", $request_params['start'])[0]));
    
                $end = join("", explode("-", explode(" ", $request_params['end'])[0]));
    
                $city = strtolower($request_params['city_object']['city']);
    
                $agent = 'all-agents';
    
                $filename = $agent ? 'report_'.$city.'_'.$agent.'_'.$start.'_'.$end.'.xlsx' : 'report_'.$city.'_'.$start.'_'.$end.'.xlsx';
                
                Excel::store(new IndividualCronExport($request_params), $filename, 'public_uploads', null, [
                    'visibility' => 'public',
                ]);
                
                $fullPath = Storage::disk('public_uploads')->path($filename);
                
                $addresses_array = $this->addresses($request_params['city_object']['city']);
                $addresses_array = $addresses_array[0]->emails;
                $mail_addressees = explode(",", $addresses_array);
                $data['filename'] = $filename;
                
                $file = public_path('reports/'.$data['filename']);
                
                for($i = 0; $i < count($mail_addressees); $i++){
                    Mail::send('mail.mail-pattern', $data, function($message)use($data, $file, $mail_addressees, $i, $request_params) {
                        $message->to($mail_addressees[$i])
                                ->subject('Промежуточный ('.$request_params['start'].' - '.$request_params['end'].') отчет от Compass, город '.strtolower($request_params['city_object']['city']).', для '.$mail_addressees[$i]);
            
                        $message->attach($file);
                    });
                }
    
                Storage::disk('public_uploads')->delete($filename);
            }

            return response()->json([
                'message' => 'Individual reports are successfully sent to addressees.'
            ], 200);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get cities
     * @return Object
     */
    public function cities(): Object
    {
        return DB::table('crm_stores as s')
            ->where('s.deleted', '=', '0')
            ->where('s.disabled', '=', '0')
            ->where('s.owner_id', '=', '6')
            ->select(
                's.city'
            )
            ->distinct()
            ->get()
            ;
    }

    /**
     * Get stores
     * @param Array $request
     * @return Object
     */
    public function stores(Array $request): Object
    {
        return DB::table('crm_stores as s')
            ->where('s.deleted', '=', '0')
            ->where('s.disabled', '=', '0')
            ->where('s.owner_id', '=', '6')
            ->where('s.city', '=', $request['city'])
            ->select(
                's.title as label',
                's.id as id'
            )
            ->distinct()
            ->get()
            ;
    }

    /**
     * Get city's mail addressees
     * @return Object
     */
    public function addresses(String $city): Object
    {
        return DB::table('individual_cron_info as ind')
            ->where('ind.city', '=', $city)
            ->select(
                'ind.emails'
            )
            ->get()
            ;
    }
}