<?php

namespace App\Exports\AurikaSheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class AurikaTransactionsSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $query = DB::table('crm_orders_nipl as n')
        ->join('crm_stores as s', 'n.store_id', '=', 's.id')
        ->join('crm_store_products as sp', 'n.product_id', '=', 'sp.id')
        ->join('crm_products as p', 'sp.product_id', '=', 'p.id')
        ->where('s.disabled', '=', '0')
        ->where('s.deleted', '=', '0')
        ->where('s.owner_id', '=', '4974')
        ->where('n.status', '=', '3')
        ->whereRaw(DB::raw('n.created_at BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'))
        ->whereIn('n.order_payment_type', $this->request['agents'])
        ->whereIn('n.store_id', $this->request['stores']);
        
        // $query = $query->where('s.city', '=', $this->request['city']);
        $query = $query->whereIn('s.city', $this->request['city']);

        $query = $query->select(
            'n.id as Номер заказа',
            'n.external_id as Номер чека',
            's.title as Наименование АЗС',
            DB::raw('round(n.delivered_litres * n.product_price)'),
            DB::raw('date_add(n.created_at, interval 5 hour)'),
            DB::raw('round(n.delivered_litres, 2)'),
            'n.product_price as Цена',
            'p.title as Тип топлива',
            DB::raw('(CASE WHEN n.order_payment_type = 8 then "halyk" WHEN n.order_payment_type = 9 then "kaspi" WHEN n.order_payment_type = 10 then "alacard" ELSE "Смартзаправка" END)'),
            'n.external_id as Номер транзакции'
        )
        ->orderBy('n.created_at', 'desc')
        ->get();

        return $query;
    }

    public function headings(): array
    {
        return ["Номер заказа", "Номер чека", "Наименование АЗС", "Оплаченная сумма", 'Дата транзакции', 'Приобретенные литры', 'Цена', 'Тип топлива', 'Агент', 'Номер транзакции'];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:I')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(18);   
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(18);  
            }
        ];
    }

    public function title(): string
    {
        return 'Транзакции';
    }
}