<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\HeliosReportSheets\HeliosReportTransactionsSheet;
use App\Exports\HeliosReportSheets\HeliosReportOverallSheet;

class HeliosReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new HeliosReportTransactionsSheet($this->request);
        $sheets[] = new HeliosReportOverallSheet($this->request);

        return $sheets;
    }
}