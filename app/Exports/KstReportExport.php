<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\KstReportSheets\KstReportTransactionsSheet;
use App\Exports\KstReportSheets\KstReportOverallSheet;

class KstReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new KstReportTransactionsSheet($this->request);
        $sheets[] = new KstReportOverallSheet($this->request);

        return $sheets;
    }
}