<?php

namespace App\Exports\VenoilReportSheets;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\CrmOrdersRetalix;
use AppModels\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\WithTitle;

class VenoilReportOverallSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $query = DB::table('crm_orders_nipl as n')
        ->join('crm_stores as s', 'n.store_id', '=', 's.id')
        ->join('crm_store_products as sp', 'n.product_id', '=', 'sp.id')
        ->join('crm_products as p', 'sp.product_id', '=', 'p.id')
        ->where('s.disabled', '=', '0')
        ->where('s.deleted', '=', '0')
        ->where('s.owner_id', '=', '9683')
        ->where('n.status', '=', '3')
        ->whereRaw(DB::raw('n.created_at BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        // ->whereIn('n.order_payment_type', $this->request['agents']);
        
        if($this->request['all_stations'] == "false"){
            // $query = $query->where('s.city', '=', $this->request['city']);
            $query = $query->whereIn('n.store_id', $this->request['stores']);
            $query = $query->whereIn('s.city', $this->request['city']);
        }
        
        if($this->request['all_stations'] == "true"){
            //do smth
        }

        $query = $query->select(
            'p.title as Тип топлива',
            DB::raw('sum(round(n.deliver_quantity * n.product_price))'),
            DB::raw('sum(round(n.deliver_quantity, 2))'),
        )->groupBy('p.title');

        $query2 = DB::table('crm_orders_nipl as n')
        ->join('crm_stores as s', 'n.store_id', '=', 's.id')
        ->join('crm_store_products as sp', 'n.product_id', '=', 'sp.id')
        ->join('crm_products as p', 'sp.product_id', '=', 'p.id')
        ->join('crm_last_message_ids as lmi', 'n.id', '=', 'lmi.order_id')
        ->where('s.disabled', '=', '0')
        ->where('s.deleted', '=', '0')
        ->where('s.owner_id', '=', '9683')
        ->where('lmi.type', '=', 'prepaidRefuelingRejected')
        // ->where('n.status', '=', '3')
        ->whereRaw(DB::raw('n.created_at BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        // ->whereIn('n.order_payment_type', $this->request['agents']);
        
        if($this->request['all_stations'] == "false"){
            // $query = $query->where('s.city', '=', $this->request['city']);
            $query2 = $query2->whereIn('n.store_id', $this->request['stores']);
            $query2 = $query2->whereIn('s.city', $this->request['city']);
        }
        
        if($this->request['all_stations'] == "true"){
            //do smth
        }

        $query2 = $query2->select(
            'p.title as Тип топлива',
            DB::raw('sum(round(n.deliver_quantity * n.product_price))'),
            DB::raw('sum(round(n.deliver_quantity, 2))'),
        )
        ->groupBy('p.title')
        ->union($query)
        ->get();

        return $query2;
    }

    public function headings(): array
    {
        return ["Тип топлива", "Общая сумма в тенге", "Общий литраж"];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:C')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18);     
            }
        ];
    }

    public function title(): string
    {
        return 'Итоги';
    }
}