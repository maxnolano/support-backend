<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\EntireAurikaKstSheets\EntireAurikaKstTransactionsSheet;
use App\Exports\EntireAurikaKstSheets\EntireAurikaKstOverallSheet;

class EntireAurikaKstExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new EntireAurikaKstTransactionsSheet($this->request);
        // $sheets[] = new EntireOverallSheet($this->request);

        return $sheets;
    }
}