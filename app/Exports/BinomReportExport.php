<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\BinomReportSheets\BinomReportTransactionsSheet;
use App\Exports\BinomReportSheets\BinomReportOverallSheet;

class BinomReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new BinomReportTransactionsSheet($this->request);
        $sheets[] = new BinomReportOverallSheet($this->request);

        return $sheets;
    }
}