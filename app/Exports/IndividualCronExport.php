<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\IndividualCronSheets\TransactionsSheet;
use App\Exports\IndividualCronSheets\OverallSheet;

class IndividualCronExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new TransactionsSheet($this->request);
        $sheets[] = new OverallSheet($this->request);

        return $sheets;
    }
}