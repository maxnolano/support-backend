<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\AurikaSheets\AurikaTransactionsSheet;
use App\Exports\AurikaSheets\AurikaOverallSheet;

class AurikaExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new AurikaTransactionsSheet($this->request);
        $sheets[] = new AurikaOverallSheet($this->request);

        return $sheets;
    }
}