<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\SinooilReportSheets\SinooilReportTransactionsSheet;
use App\Exports\SinooilReportSheets\SinooilReportOverallSheet;

class SinooilReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new SinooilReportTransactionsSheet($this->request);
        $sheets[] = new SinooilReportOverallSheet($this->request);

        return $sheets;
    }
}