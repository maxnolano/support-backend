<?php

namespace App\Exports\IndividualCronSheets;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\WithTitle;

class OverallSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $query = DB::table('crm_orders_retalix as r')
        ->join('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
        ->join('crm_stores as s', 'r.store_id', '=', 's.id')
        ->where('r.system_type', '=', '1')
        ->where('s.owner_id', '=', '6')
        ->where('r.status', '=', '5')
        ->whereRaw(DB::raw('c.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'))
        ->whereIn('r.payment_type', $this->request['agents'])
        ->whereIn('r.store_id', $this->request['stores']);
        
        $query = $query->where('s.city', '=', $this->request['city_object']['city']);

        $query = $query->select(
            'r.product_title as Тип топлива',
            DB::raw('sum(round(c.litre * r.product_price))'),
            DB::raw('sum(round(c.litre, 2))'),
        )
        ->groupBy('r.product_title')
        ->get();

        return $query;
    }

    public function headings(): array
    {
        return ["Тип топлива", "Общая сумма в тенге", "Общий литраж"];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:C')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18);     
            }
        ];
    }

    public function title(): string
    {
        return 'Итоги';
    }
}