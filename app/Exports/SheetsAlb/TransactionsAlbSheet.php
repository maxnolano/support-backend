<?php

namespace App\Exports\SheetsAlb;

use Maatwebsite\Excel\Concerns\FromQuery;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class TransactionsAlbSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        
        $cities_not_excepted = [];
        $cities_in_exception = [];        
        foreach($this->request['city'] as $city){
            if($city == 'Kyzylorda' || $city == 'Aktobe'){
                $cities_in_exception[] = $city;
                // Log::info($city);
                // Log::info('1st condition ----------------------------------------------');
            }else{
                $cities_not_excepted[] = $city;
                // Log::info($city);
                // Log::info('2nd condition ----------------------------------------------');
            }
        }

        //-----------------------------
        //CASE FOR OTHER CITIES (EXCEPT AKTOBE AND KYZYLORDA)
        //-----------------------------

        $query = DB::table('crm_orders_retalix as r')//everything except aktobe and kyzylorda3
            ->join('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
            ->join('crm_stores as s', 'r.store_id', '=', 's.id')
            ->join('crm_orders_nipl as npl', 'r.nipl_order_id', '=', 'npl.id')
            ->where('r.system_type', '=', '1')
            ->where('s.owner_id', '=', '6')
            ->whereIn('r.payment_type', $this->request['agents'])
            ->whereIn('r.store_id', $this->request['stores']);
        
        $query = $query->whereRaw(DB::raw('c.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        
        $query = $query->whereIn('s.city', $cities_not_excepted);

        if($this->request['columnAgent'] == 'false'){            
            $query = $query->select(
                'r.nipl_order_id as Номер заказа',
                'c.ext_order_id as Номер чека',
                's.title as Наименование АЗС',
                DB::raw('round(c.litre * r.product_price)'),
                DB::raw('date_add(c.create_date, interval 5 hour)'),
                DB::raw('round(c.litre, 2)'),
                'r.product_price as Цена',
                'r.product_title as Тип топлива',
                'npl.external_id as Номер транзакции',
                DB::raw('date_add(c.create_date, interval 5 hour)')
            )
            ->orderBy('c.create_date', 'desc');
        }else{
            $query = $query->select(
                'r.nipl_order_id as Номер заказа',
                'c.ext_order_id as Номер чека',
                's.title as Наименование АЗС',
                DB::raw('round(c.litre * r.product_price)'),
                DB::raw('date_add(c.create_date, interval 5 hour)'),
                DB::raw('round(c.litre, 2)'),
                'r.product_price as Цена',
                'r.product_title as Тип топлива',
                'npl.external_id as Номер транзакции',
                DB::raw('date_add(c.create_date, interval 5 hour)'),
                DB::raw('(CASE WHEN r.payment_type = 8 then "halyk" WHEN r.payment_type = 9 then "kaspi" WHEN r.payment_type = 10 then "alacard" ELSE "Смартзаправка" END)')
            )
            ->orderBy('c.create_date', 'desc');
        }

        //-----------------------------
        //CASE FOR KYZYLORDA AND AKTOBE
        //-----------------------------

        $query2 = DB::table('crm_orders_retalix as r')//everything except aktobe and kyzylorda3
            ->join('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
            ->join('crm_stores as s', 'r.store_id', '=', 's.id')
            ->join('crm_orders_nipl as npl', 'r.nipl_order_id', '=', 'npl.id')
            ->where('r.system_type', '=', '1')
            ->where('s.owner_id', '=', '6')
            ->whereIn('r.payment_type', $this->request['agents'])
            ->whereIn('r.store_id', $this->request['stores']);
        
        $query2 = $query2->whereRaw(DB::raw('c.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        
        $query2 = $query2->whereIn('s.city', $cities_in_exception);

        if($this->request['columnAgent'] == 'false'){            
            $query2 = $query2->select(
                'r.nipl_order_id as Номер заказа',
                'c.ext_order_id as Номер чека',
                's.title as Наименование АЗС',
                DB::raw('round(c.litre * r.product_price)'),
                DB::raw('date_add(c.create_date, interval 5 hour)'),
                DB::raw('round(c.litre, 2)'),
                'r.product_price as Цена',
                'r.product_title as Тип топлива',
                'npl.external_id as Номер транзакции',
                DB::raw('date_add(c.create_date, interval 5 hour)')
            )
            ->orderBy('c.create_date', 'desc')
            ->union($query)
            ->get();
        }else{
            $query2 = $query2->select(
                'r.nipl_order_id as Номер заказа',
                'c.ext_order_id as Номер чека',
                's.title as Наименование АЗС',
                DB::raw('round(c.litre * r.product_price)'),
                DB::raw('date_add(c.create_date, interval 5 hour)'),
                DB::raw('round(c.litre, 2)'),
                'r.product_price as Цена',
                'r.product_title as Тип топлива',
                'npl.external_id as Номер транзакции',
                DB::raw('date_add(c.create_date, interval 5 hour)'),
                DB::raw('(CASE WHEN r.payment_type = 8 then "halyk" WHEN r.payment_type = 9 then "kaspi" WHEN r.payment_type = 10 then "alacard" ELSE "Смартзаправка" END)')
            )
            ->orderBy('c.create_date', 'desc')
            ->union($query)
            ->get();
        }

        return $query2;
    }

    public function headings(): array
    {
        if($this->request['columnAgent'] == 'false'){
            return ["Номер заказа", "Номер чека", "Наименование АЗС", "Оплаченная сумма", 'Дата транзакции', 'Приобретенные литры', 'Цена', 'Тип топлива', 'Номер транзакции', 'Время Астаны'];
        }else{
            return ["Номер заказа", "Номер чека", "Наименование АЗС", "Оплаченная сумма", 'Дата транзакции', 'Приобретенные литры', 'Цена', 'Тип топлива', 'Номер транзакции', 'Время Астаны', 'Агент'];
        }
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                if($this->request['columnAgent'] == 'false'){
                    $event->sheet->getDelegate()->getStyle('A:J')
                                    ->getAlignment()
                                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                }else{
                    $event->sheet->getDelegate()->getStyle('A:K')
                                    ->getAlignment()
                                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                }

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(18);
                
                if($this->request['columnAgent'] == 'false'){
                    $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(18);   
                }  
            }
        ];
    }

    public function title(): string
    {
        return 'Транзакции';
    }
}