<?php

namespace App\Exports\SheetsAlb;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\WithTitle;

class OverallAlbSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $cities_not_excepted = [];
        $cities_in_exception = [];        
        foreach($this->request['city'] as $city){
            if($city == 'Kyzylorda' || $city == 'Aktobe'){
                $cities_in_exception[] = $city;
            }else{
                $cities_not_excepted[] = $city;
            }
        }

        //-----------------------------
        //CASE FOR OTHER CITIES (EXCEPT AKTOBE AND KYZYLORDA)
        //-----------------------------

        $query = DB::table('crm_orders_retalix as r')
            ->join('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
            ->join('crm_stores as s', 'r.store_id', '=', 's.id')
            ->where('r.system_type', '=', '1')
            ->where('s.owner_id', '=', '6')
            ->whereIn('r.payment_type', $this->request['agents'])
            ->whereIn('r.store_id', $this->request['stores']);

        $query = $query->whereRaw(DB::raw('c.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        
        $query = $query->whereIn('s.city', $cities_not_excepted);

        $query = $query->select(
            'r.product_title as Тип топлива',
            DB::raw('sum(round(c.litre * r.product_price))'),
            DB::raw('sum(round(c.litre, 2))'),
        )
        ->groupBy('r.product_title');

        //-----------------------------
        //CASE FOR KYZYLORDA AND AKTOBE
        //-----------------------------

        $query2 = DB::table('crm_orders_retalix as r')
            ->join('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
            ->join('crm_stores as s', 'r.store_id', '=', 's.id')
            ->where('r.system_type', '=', '1')
            ->where('s.owner_id', '=', '6')
            ->whereIn('r.payment_type', $this->request['agents'])
            ->whereIn('r.store_id', $this->request['stores']);

        $query2 = $query2->whereRaw(DB::raw('c.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        
        $query2 = $query2->whereIn('s.city', $cities_in_exception);

        $query2 = $query2->select(
            'r.product_title as Тип топлива',
            DB::raw('sum(round(c.litre * r.product_price))'),
            DB::raw('sum(round(c.litre, 2))'),
        )
        ->groupBy('r.product_title')
        ->union($query)
        ->get();

        return $query2;
    }

    public function headings(): array
    {
        return ["Тип топлива", "Общая сумма в тенге", "Общий литраж"];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:C')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18);     
            }
        ];
    }

    public function title(): string
    {
        return 'Итоги';
    }
}