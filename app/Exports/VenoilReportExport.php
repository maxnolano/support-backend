<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\VenoilReportSheets\VenoilReportTransactionsSheet;
use App\Exports\VenoilReportSheets\VenoilReportOverallSheet;

class VenoilReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new VenoilReportTransactionsSheet($this->request);
        $sheets[] = new VenoilReportOverallSheet($this->request);

        return $sheets;
    }
}