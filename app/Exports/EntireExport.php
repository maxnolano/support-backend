<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\EntireSheets\EntireTransactionsSheet;
use App\Exports\EntireSheets\EntireOverallSheet;

class EntireExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new EntireTransactionsSheet($this->request);
        // $sheets[] = new EntireOverallSheet($this->request);

        return $sheets;
    }
}