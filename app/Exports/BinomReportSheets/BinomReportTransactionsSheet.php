<?php

namespace App\Exports\BinomReportSheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class BinomReportTransactionsSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $query = DB::table('crm_orders_nipl as n')
        ->join('crm_stores as s', 'n.store_id', '=', 's.id')
        ->join('crm_store_products as sp', 'n.product_id', '=', 'sp.id')
        ->join('crm_products as p', 'sp.product_id', '=', 'p.id')
        // ->join('crm_last_message_ids as lmi', 'n.id', '=', 'lmi.order_id')
        ->where('s.disabled', '=', '0')
        ->where('s.deleted', '=', '0')
        ->where('s.owner_id', '=', '9652')
        // ->where('lmi.type', '=', 'prepaidRefuelingRejected')
        ->where('n.status', '=', '3')
        ->whereRaw(DB::raw('n.created_at BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        // ->whereIn('n.order_payment_type', $this->request['agents']);
        
        if($this->request['all_stations'] == "false"){
            // $query = $query->where('s.city', '=', $this->request['city']);
            $query = $query->whereIn('n.store_id', $this->request['stores']);
            $query = $query->whereIn('s.city', $this->request['city']);
        }
        
        if($this->request['all_stations'] == "true"){
            //do smth
        }

        $query = $query->select(
            'n.id as Номер заказа',
            'n.batch_number as Номер чека',
            's.title as Наименование АЗС',
            // DB::raw('round(n.deliver_quantity * n.product_price)'), old оплаченная сумма
            'n.accept_amt',
            DB::raw('date_add(n.created_at, interval 5 hour)'),
            DB::raw('round(n.deliver_quantity, 2)'),
            'n.product_price as Цена',
            'p.title as Тип топлива',
            DB::raw('(CASE WHEN n.order_payment_type = 8 then "halyk" WHEN n.order_payment_type = 9 then "kaspi" WHEN n.order_payment_type = 10 then "alacard" ELSE "Смартзаправка" END)'),
            'n.external_id as external_id',
            'n.title as guid',
            // 'lmi.cancel_reason as reason',
            'n.status as status',
            'n.order_payment_type'
        );

        $query2 = DB::table('crm_orders_nipl as n')
        ->join('crm_stores as s', 'n.store_id', '=', 's.id')
        ->join('crm_store_products as sp', 'n.product_id', '=', 'sp.id')
        ->join('crm_products as p', 'sp.product_id', '=', 'p.id')
        ->join('crm_last_message_ids as lmi', 'n.id', '=', 'lmi.order_id')
        ->where('s.disabled', '=', '0')
        ->where('s.deleted', '=', '0')
        ->where('s.owner_id', '=', '9652')
        ->where('lmi.type', '=', 'prepaidRefuelingRejected')
        // ->where('n.status', '=', '3')
        ->whereRaw(DB::raw('n.created_at BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'));
        // ->whereIn('n.order_payment_type', $this->request['agents']);
        
        if($this->request['all_stations'] == "false"){
            // $query = $query->where('s.city', '=', $this->request['city']);
            $query2 = $query2->whereIn('n.store_id', $this->request['stores']);
            $query2 = $query2->whereIn('s.city', $this->request['city']);
        }
        
        if($this->request['all_stations'] == "true"){
            //do smth
        }

        $query2 = $query2->select(
            'n.id as Номер заказа',
            'n.batch_number as Номер чека',
            's.title as Наименование АЗС',
            // DB::raw('round(n.deliver_quantity * n.product_price)'), old оплаченная сумма
            'n.accept_amt',
            DB::raw('date_add(n.created_at, interval 5 hour)'),
            DB::raw('round(n.deliver_quantity, 2)'),
            'n.product_price as Цена',
            'p.title as Тип топлива',
            DB::raw('(CASE WHEN n.order_payment_type = 8 then "halyk" WHEN n.order_payment_type = 9 then "kaspi" WHEN n.order_payment_type = 10 then "alacard" ELSE "Смартзаправка" END)'),
            'n.external_id as external_id',
            'n.title as guid',
            'lmi.cancel_reason as reason',
            'n.order_payment_type'
        )
        // ->orderBy('n.created_at', 'desc')
        ->union($query)
        ->get();

        return $query2;
    }

    public function headings(): array
    {
        return ["Номер заказа", "Номер чека", "Наименование АЗС", "Оплаченная сумма", 'Дата транзакции', 'Приобретенные литры', 'Цена', 'Тип топлива', 'Агент', 'External_id', 'GUID', 'Reason', 'Payment_type'];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:M')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(18);     
                $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(36);
                $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(36);
                $event->sheet->getDelegate()->getColumnDimension('M')->setWidth(36);     
            }
        ];
    }

    public function title(): string
    {
        return 'Транзакции';
    }
}