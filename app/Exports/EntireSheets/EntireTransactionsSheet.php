<?php

namespace App\Exports\EntireSheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class EntireTransactionsSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $query = DB::table('crm_orders_retalix as r')
        ->leftJoin('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
        ->join('crm_stores as s', 'r.store_id', '=', 's.id')
        ->join('crm_orders_nipl as npl', 'r.nipl_order_id', '=', 'npl.id')
        ->where('r.system_type', '=', '1')
        ->where('s.owner_id', '=', '6')
        ->whereRaw(DB::raw('r.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'))
        ->whereIn('r.payment_type', $this->request['agents'])
        ->whereIn('r.store_id', $this->request['stores']);
        
        $query = $query->whereIn('s.city', $this->request['city']);

        if(!empty($this->request['statuses'])){
            $query = $query->whereIn('r.status', $this->request['statuses']);
        }

        if(!empty($this->request['check_num'])){
            $query = $query->where('c.ext_order_id', '=', $this->request['check_num']);
        }

        if(!empty($this->request['transaction_num'])){
            $query = $query->where('r.id', '=', $this->request['transaction_num']);
        }

        $query = $query->select(
            'r.nipl_order_id as Номер заказа',
            'c.ext_order_id as Номер чека',
            's.title as Наименование АЗС',
            // DB::raw('round(c.litre * r.product_price)'), //npl.total_order_amt
            'npl.total_order_amt',
            DB::raw('date_add(r.create_date, interval 5 hour)'),
            // DB::raw('ifnull (round(c.litre, 2), r.demand_quantity)'),
            'npl.demand_quantity',
            'r.product_price as Цена',
            'r.product_title as Тип топлива',
            DB::raw('(CASE WHEN r.payment_type = 8 then "halyk" WHEN r.payment_type = 9 then "kaspi" WHEN r.payment_type = 10 then "alacard" ELSE "Смартзаправка" END)'),
            DB::raw('(CASE WHEN r.status = 1 then "1 - AccepOrder" WHEN r.status = 2 then "2 - WaitingRefueling" WHEN r.status = 3 then "3 - Fueling" WHEN r.status = 4 then "4 - Canceled" WHEN r.status = 5 then "5 - Completed" ELSE "6 - Expire" END)'),
            'npl.external_id as Номер транзакции',
            'npl.accept_amt',
            'npl.deliver_quantity',
            'c.ext_date',
        )
        ->orderBy('r.create_date', 'desc')
        ->get();

        return $query;
    }

    public function headings(): array
    {
        return ["Номер заказа", "Номер чека", "Наименование АЗС", "Оплаченная сумма", 'Дата транзакции', 'Заказано литров', 'Цена', 'Тип топлива', 'Агент', 'Статус', 'Номер транзакции', 'Списанная сумма', 'Отпущено литров', 'ext_date'];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:N')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(18); 
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('M')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('N')->setWidth(18);
            }
        ];
    }

    public function title(): string
    {
        return 'Транзакции';
    }
}