<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\CorpReportSheets\CorpReportTransactionsSheet;
use App\Exports\CorpReportSheets\CorpReportOverallSheet;

class CorpReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new CorpReportTransactionsSheet($this->request);
        $sheets[] = new CorpReportOverallSheet($this->request);

        return $sheets;
    }
}