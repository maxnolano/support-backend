<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersNipl;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\M36ReportSheets\M36ReportTransactionsSheet;
use App\Exports\M36ReportSheets\M36ReportOverallSheet;

class M36ReportExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new M36ReportTransactionsSheet($this->request);
        $sheets[] = new M36ReportOverallSheet($this->request);

        return $sheets;
    }
}