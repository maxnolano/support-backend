<?php

namespace App\Exports\EntireAurikaKstSheets;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\WithTitle;

class EntireAurikaKstOverallSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(in_array("1", $this->request['agents'])){
            $this->request['agents'][] = 0;
            $this->request['agents'][] = 5;
            $this->request['agents'][] = 6;
        }

        $query = DB::table('crm_orders_retalix as r')
        ->leftJoin('crm_orders_retalix_cancelled_completed as c', 'r.id', '=', 'c.order_id')
        ->join('crm_stores as s', 'r.store_id', '=', 's.id')
        ->where('r.system_type', '=', '1')
        ->where('s.owner_id', '=', '4974')
        ->whereRaw(DB::raw('r.create_date BETWEEN DATE_ADD(\''.$this->request['start'].'\', INTERVAL -5 HOUR) AND DATE_ADD(\''.$this->request['end'].'\', INTERVAL -5 HOUR)'))
        ->whereIn('r.payment_type', $this->request['agents'])
        ->whereIn('r.store_id', $this->request['stores']);
        
        $query = $query->whereIn('s.city', $this->request['city']);

        if(!empty($this->request['statuses'])){
            $query = $query->whereIn('r.status', $this->request['statuses']);
        }

        if(!empty($this->request['check_num'])){
            $query = $query->where('c.ext_order_id', '=', $this->request['check_num']);
        }

        if(!empty($this->request['transaction_num'])){
            $query = $query->where('r.id', '=', $this->request['transaction_num']);
        }

        $query = $query->select(
            'r.product_title as Тип топлива',
            DB::raw('ifnull (sum(round(c.litre * r.product_price)), round(r.demand_quantity))'),
            DB::raw('ifnull (sum(round(c.litre, 2)), round(r.demand_quantity))'),
        )
        ->groupBy('r.product_title')
        ->get();

        return $query;
    }

    public function headings(): array
    {
        return ["Тип топлива", "Общая сумма в тенге", "Общий литраж"];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:C')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18);     
            }
        ];
    }

    public function title(): string
    {
        return 'Итоги';
    }
}