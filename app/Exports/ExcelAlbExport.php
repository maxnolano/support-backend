<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CrmOrdersRetalix;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\SheetsAlb\TransactionsAlbSheet;
use App\Exports\SheetsAlb\OverallAlbSheet;

class ExcelAlbExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new TransactionsAlbSheet($this->request);
        $sheets[] = new OverallAlbSheet($this->request);

        return $sheets;
    }
}