<?php

namespace App\Repositories\M36Report;

use App\Repositories\Interfaces\M36ReportRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use Illuminate\Support\Facades\DB;

class M36ReportRepository implements M36ReportRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Get the excel report
     * @param Array $request
     * @param int $system_type
     * @return Object
     */
    public function report(Array $request): Object
    {
        // todo
    }

    /**
     * Get cities for this endpoint
     * @return Object
     */
    public function cities(): Object
    {
        return DB::table('crm_stores as s')
            ->where('s.deleted', '=', '0')
            ->where('s.disabled', '=', '0')
            ->where('s.owner_id', '=', '9592')
            ->select(
                's.city'
            )
            ->distinct()
            ->get()
            ;
    }

    /**
     * Get stores for this endpoint
     * @param Array $request
     * @return Object
     */
    public function stores(Array $request): Object
    {
        return DB::table('crm_stores as s')
            ->where('s.deleted', '=', '0')
            ->where('s.disabled', '=', '0')
            ->where('s.owner_id', '=', '9592')
            // ->where('s.city', '=', $request['city'])
            ->whereIn('s.city', $request['city'])
            ->select(
                's.title as label',
                's.id as id'
            )
            ->distinct()
            ->get()
            ;
    }
}