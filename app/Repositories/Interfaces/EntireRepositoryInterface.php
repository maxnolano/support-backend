<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface EntireRepositoryInterface
{
    /**
     * Get the excel report
     * @param Array $request
     * @return Object
     */
    public function report(Array $request): Object;

    /**
     * Get cities for this endpoint
     * @return Object
     */
    public function cities(): Object;

    /**
     * Get stores for this endpoint
     * @param Array $request
     * @return Object
     */
    public function stores(Array $request): Object;

    /**
     * Get statuses for this endpoint
     * @return Object
     */
    public function statuses(): Object;
}