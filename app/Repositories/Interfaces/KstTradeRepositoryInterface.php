<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface KstTradeRepositoryInterface
{
    /**
     * Getting list of orders
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function orders(Array $request): LengthAwarePaginator;

    /**
     * Exact order
     * @param Array $request
     * @return Object
     */
    public function order(Array $request): Object;

    /**
     * Update order
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array;

    /**
     * Update status (cancelling)
     * @param Array $request
     * @return Object
     */
    public function cancel(Array $request): array;

    /**
     * Update status (refunding)
     * @param Array $request
     * @return Object
     */
    public function refund(Array $request): array;
}