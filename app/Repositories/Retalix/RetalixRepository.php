<?php


namespace App\Repositories\Retalix;

use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersRetalixCancelledCompleted;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\RetalixRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};

class RetalixRepository implements RetalixRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of orders
     * @param Array $request
     * @param int $system_type
     * @return LengthAwarePaginator
     */
    public function orders(Array $request, int $system_type): LengthAwarePaginator
    {
        return DB::table('crm_orders_retalix as or')
            ->join('crm_stores as st', 'or.store_id', '=', 'st.id')
            // ->join('crm_orders_nipl as ordn', 'or.nipl_order_id', '=', 'ordn.id')
            ->where('or.system_type', '=', $system_type)
            ->where('or.id', 'like', '%'.$request['id'].'%')
            ->where('or.store_id', 'like', '%'.$request['store_id'].'%')
            ->where('st.title', 'like', '%'.$request['st_title'].'%')
            ->where('or.pump_number', 'like', '%'.$request['pump_number'].'%')
            ->where('or.product_price', 'like', '%'.$request['product_price'].'%')
            ->where('or.payment_type', 'like', '%'.$request['payment_type'].'%')
            ->where('or.status', 'like', '%'.$request['status'].'%')
            ->where('or.create_date', 'like', '%'.$request['create_date'].'%')
            ->where('or.modify_date', 'like', '%'.$request['modify_date'].'%')
            ->where('or.nipl_order_id', 'like', '%'.$request['nipl_order_id'].'%')
            ->where('or.total_order_amt', 'like', '%'.$request['total_order_amt'].'%')
            // ->where('ordn.external_id', 'like', '%'.$request['external_id'].'%')
            ->select(
                'or.id as id',
                'or.store_id as store_id',
                'or.pump_number as pump_number',
                'or.product_price as product_price',
                'or.payment_type as payment_type',
                'or.status as status',
                'or.create_date as create_date',
                'or.modify_date as modify_date',
                'or.system_type as system_type',
                'st.title as st_title',
                'or.nipl_order_id',
                'or.total_order_amt',
                // 'ordn.external_id',
                'or.product_id'
            )
            ->orderBy('or.create_date', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Exact order
     * @param Array $request
     * @param int $system_type
     * @return Object
     */
    public function order(Array $request, int $system_type): Object
    {
        return DB::table('crm_orders_retalix as or')
            ->join('crm_orders_retalix_cancelled_completed as orcc', 'or.id', '=', 'orcc.order_id')
            ->join('crm_stores as st', 'orcc.store_id', '=', 'st.id')
            ->where('or.id', '=', trim($request['id']))
            ->where('orcc.order_id', '=', trim($request['id']))
            ->where('or.system_type', '=', $system_type)
            ->whereIn('or.status', [4, 5])
            ->whereIn('orcc.status', [4,5])
            ->select(
                'orcc.id as c_id',
                'orcc.order_id as c_order_id',
                'orcc.litre as c_litre',
                'orcc.store_id as c_store_id',
                'orcc.cancel_reason as c_cancel_reason',
                'orcc.ext_order_id as c_ext_order_id',
                'orcc.ext_date as c_ext_date',
                'orcc.create_date as c_create_date',
                'orcc.status as c_status',
                'st.title as st_title'
            )
            ->get();
    }

    /**
     * Update order
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array
    {
        $retalix_status = DB::table('crm_orders_retalix')
            ->where('id', $request['id'])
            ->update(['status' => $request['value']]);

        $retalix_cancelled_status = DB::table('crm_orders_retalix_cancelled_completed')
            ->where('order_id', $request['id'])
            ->update(['status' => $request['value']]);

        return ['retalix_status' => $retalix_status, 'retalix_cancelled_status' => $retalix_cancelled_status];
    }

    /**
     * Update status (cancelling)
     * @param Array $request
     * @return Object
     */
    public function cancel(Array $request): array
    {
        //todo
    }

    /**
     * Update status (refunding)
     * @param Array $request
     * @return Object
     */
    public function refund(Array $request): array
    {
        //todo
    }
}