<?php


namespace App\Repositories\M36Orders;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\M36OrdersRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use App\Models\CrmOrdersNipl;

class M36OrdersRepository implements M36OrdersRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of orders
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function orders(Array $request): LengthAwarePaginator
    {
        return DB::table('crm_orders_nipl as or')
            ->join('crm_stores as st', 'or.store_id', '=', 'st.id')
            ->where('st.owner_id', '=', '9592')
            ->where('or.id', 'like', '%'.$request['id'].'%')
            ->where('or.store_id', 'like', '%'.$request['store_id'].'%')
            ->where('st.title', 'like', '%'.$request['st_title'].'%')
            ->where('or.pump_number', 'like', '%'.$request['pump_number'].'%')
            ->where('or.product_price', 'like', '%'.$request['product_price'].'%')
            ->where('or.order_payment_type', 'like', '%'.$request['order_payment_type'].'%')
            ->where('or.status', 'like', '%'.$request['status'].'%')
            ->where('or.created_at', 'like', '%'.$request['created_at'].'%')
            ->where('or.updated_at', 'like', '%'.$request['updated_at'].'%')
            ->where('or.total_order_amt', 'like', '%'.$request['total_order_amt'].'%')
            ->where('or.external_id', 'like', '%'.$request['external_id'].'%')
            ->select(
                'or.id as id',
                'or.store_id as store_id',
                'or.pump_number as pump_number',
                'or.product_price as product_price',
                'or.order_payment_type as order_payment_type',
                'or.status as status',
                'or.created_at as created_at',
                'or.updated_at as updated_at',
                'st.title as st_title',
                'or.total_order_amt',
                'or.external_id',
                'or.product_id'
            )
            ->orderBy('or.created_at', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Exact order
     * @param Array $request
     * @return Object
     */
    public function order(Array $request): Object
    {
        return DB::table('crm_orders_nipl as or')
            ->join('crm_orders_retalix as orr', 'or.id', '=', 'orr.nipl_order_id')
            ->join('crm_orders_retalix_cancelled_completed as orcc', 'orr.id', '=', 'orcc.order_id')
            ->join('crm_stores as st', 'orcc.store_id', '=', 'st.id')
            ->where('or.id', '=', trim($request['id']))
            ->where('orcc.order_id', '=', trim($request['id']))
            ->whereIn('or.status', [4, 5])
            ->whereIn('orcc.status', [4,5])
            ->select(
                'orcc.id as c_id',
                'orcc.order_id as c_order_id',
                'orcc.litre as c_litre',
                'orcc.store_id as c_store_id',
                'orcc.cancel_reason as c_cancel_reason',
                'orcc.ext_order_id as c_ext_order_id',
                'orcc.ext_date as c_ext_date',
                'orcc.create_date as c_create_date',
                'orcc.status as c_status',
                'st.title as st_title'
            )
            ->get();
    }

    /**
     * Update order
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array
    {
        $m36_status = DB::table('crm_orders_nipl')
            ->where('id', $request['id'])
            ->update(['status' => $request['value']]);

        $orderId = DB::table('crm_orders_nipl as or')
            ->join('crm_orders_retalix as orr', 'or.id', '=', 'orr.nipl_order_id')
            ->join('crm_orders_retalix_cancelled_completed as orcc', 'orr.id', '=', 'orcc.order_id')
            ->join('crm_stores as st', 'orcc.store_id', '=', 'st.id')
            ->where('or.id', '=', $request['id'])
            ->select(
                'orr.id as id'
            )
            ->first();

        if(!empty($orderId['id'])){
            $m36_cancelled_status = DB::table('crm_orders_retalix_cancelled_completed')
                ->where('order_id', $orderId['id'])
                ->update(['status' => $request['value']]);
        }else{
            $m36_cancelled_status = 'Not found';
        }

        return ['M36_status' => $m36_status, 'M36_cancelled_status' => $m36_cancelled_status];
    }

    /**
     * Update status (cancelling)
     * @param Array $request
     * @return Object
     */
    public function cancel(Array $request): array
    {
        //todo
    }

    /**
     * Update status (refunding)
     * @param Array $request
     * @return Object
     */
    public function refund(Array $request): array
    {
        //todo
    }
}