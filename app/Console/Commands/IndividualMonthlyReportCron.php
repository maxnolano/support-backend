<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Individual\IndividualCronService;

class IndividualMonthlyReportCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'indmonthlyreport:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends individual reports on a monthly basis interval to responding addressees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_start = date('Y-m-d', strtotime("first day of -1 month")); 
        $date_last = date('Y-m-d', strtotime("last day of -1 month"));
        $request = [];
        $request['start'] = $date_start.' 00:00:00';
        $request['end'] = $date_last.' 23:59:59';
        (new IndividualCronService($request))->report();
    }
}
