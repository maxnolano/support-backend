<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Services\HeliosReport\HeliosReportService;

class HeliosReportController extends ApiController
{

    /**
     * Get the excel report
     * @param Request $request
     * @return Object
     */
    public function report(Request $request): object {
        $response = (new HeliosReportService($request))->report();
        return $this->result($response);
    }

    /**
     * Send excel report via mail
     * @param Request $request
     * @return Array
     */
    public function sendReport(Request $request): Array {
        $response = (new HeliosReportService($request))->sendReport();
        return $this->result($response);
    }

    /**
     * Get cities for this endpoint
     * @param Request $request
     * @return Array
     */
    public function cities(Request $request): Array {
        $response = (new HeliosReportService($request))->cities();
        return $this->result($response);
    }

    /**
     * Get stores for this endpoint
     * @param Request $request
     * @return Array
     */
    public function stores(Request $request): Array {
        $response = (new HeliosReportService($request))->stores();
        return $this->result($response);
    }
}