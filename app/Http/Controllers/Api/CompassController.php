<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersRetalixCancelledCompleted;
use App\Http\Resources\CrmOrdersRetalix as RetalixResource;
use App\Http\Resources\CrmOrdersRetalixCancelledCompleted as RetalixCancelledResource;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Retalix\RetalixService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CompassController extends ApiController
{
    /**
     * List of orders
     * @param Request $request
     * @return Object
     */
    public function orders(Request $request): Object {
        $response = (new RetalixService($request))->orders(2);
        return $this->result($response);
    }

    /**
     * Exact order
     * @param Request $request
     * @return Object
     */
    public function order(Request $request): array {
        $response = (new RetalixService($request))->order(2);
        return $this->result($response);
    }

    /**
     * Update order
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new RetalixService($request))->update();
        return $this->result($response);
    }

    /**
     * Update status (canceling)
     * @param Request $request
     * @return Object
     */
    public function cancel(Request $request): array {
        $response = (new RetalixService($request))->cancel();
        return $this->result($response);
    }

    /**
     * Update status (refunding)
     * @param Request $request
     * @return Object
     */
    public function refund(Request $request): array {
        $response = (new RetalixService($request))->refund();
        return $this->result($response);
    }
}