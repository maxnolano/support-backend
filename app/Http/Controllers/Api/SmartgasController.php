<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\CrmOrdersRetalix;
use App\Models\CrmOrdersRetalixCancelledCompleted;
use App\Http\Resources\CrmOrdersRetalix as RetalixResource;
use App\Http\Resources\CrmOrdersRetalixCancelledCompleted as RetalixCancelledResource;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Retalix\RetalixService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SmartgasController extends ApiController
{

    /**
     * List of orders
     * @param Request $request
     * @return Object
     */
    public function orders(Request $request): Object {
        $response = (new RetalixService($request))->orders(1);
        return $this->result($response);
    }

    /**
     * Exact order
     * @param Request $request
     * @return Object
     */
    public function order(Request $request): array {
        $response = (new RetalixService($request))->order(1);
        return $this->result($response);
    }

    /**
     * Update order
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new RetalixService($request))->update();
        return $this->result($response);
    }
}