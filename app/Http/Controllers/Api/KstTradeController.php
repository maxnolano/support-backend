<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\KstTrade\KstTradeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class KstTradeController extends ApiController
{
    /**
     * List of orders
     * @param Request $request
     * @return Object
     */
    public function orders(Request $request): Object {
        $response = (new KstTradeService($request))->orders();
        return $this->result($response);
    }

    /**
     * Exact order
     * @param Request $request
     * @return Object
     */
    public function order(Request $request): array {
        $response = (new KstTradeService($request))->order();
        return $this->result($response);
    }

    /**
     * Update order
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new KstTradeService($request))->update();
        return $this->result($response);
    }

    /**
     * Update status (canceling)
     * @param Request $request
     * @return Object
     */
    public function cancel(Request $request): array {
        $response = (new KstTradeService($request))->cancel();
        return $this->result($response);
    }

    /**
     * Update status (refunding)
     * @param Request $request
     * @return Object
     */
    public function refund(Request $request): array {
        $response = (new KstTradeService($request))->refund();
        return $this->result($response);
    }
}