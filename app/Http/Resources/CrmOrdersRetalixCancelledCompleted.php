<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmOrdersRetalixCancelledCompleted extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'litre' => $this->litre,
            'store_id' => $this->store_id,
            'cancel_reason' => $this->cancel_reason,
            'ext_order_id' => $this->ext_order_id,
            'ext_date' => $this->ext_date,
            'create_date' => $this->create_date,
            'status' => $this->status,
        ];
    }
}
