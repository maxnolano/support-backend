<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\SmartgasController;
use App\Http\Controllers\Api\CompassController;
use App\Http\Controllers\Api\KstTradeController;
use App\Http\Controllers\Api\M36OrdersController;
use App\Http\Controllers\Api\KstReportController;
use App\Http\Controllers\Api\M36ReportController;
use App\Http\Controllers\Api\IndividualController;
use App\Http\Controllers\Api\IndividualAlbController;
use App\Http\Controllers\Api\AurikaController;
use App\Http\Controllers\Api\EntireController;
use App\Http\Controllers\Api\EntireAurikaKstController;
use App\Http\Controllers\Api\SinooilReportController;
use App\Http\Controllers\Api\HeliosReportController;
use App\Http\Controllers\Api\VenoilReportController;
use App\Http\Controllers\Api\BinomReportController;
use App\Http\Controllers\Api\CorpReportController;

Route::group(['namespace' => 'Api', 'prefix' => 'auth'], function(){
	Route::post('/register', [AuthController::class, 'register']);
	Route::post('/login', [AuthController::class, 'login']);
	Route::get('/user', [AuthController::class, 'user']);
	Route::post('/logout', [AuthController::class, 'logout']);
});

Route::group(['namespace' => 'Api', 'prefix' => 'smartgas'], function(){
    Route::get('/orders', [SmartgasController::class, 'orders'])->middleware('auth:api');
	Route::get('/order/{id}', [SmartgasController::class, 'order'])->middleware('auth:api');
	Route::patch('/order/{id}', [SmartgasController::class, 'update'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'compass'], function(){
    Route::get('/orders', [CompassController::class, 'orders'])->middleware('auth:api');
	Route::get('/order/{id}', [CompassController::class, 'order'])->middleware('auth:api');
	Route::patch('/order/{id}', [CompassController::class, 'update'])->middleware('auth:api');
	Route::post('/cancel', [CompassController::class, 'cancel'])->middleware('auth:api');
	Route::post('/refund', [CompassController::class, 'refund'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'ksttrade'], function(){
    Route::get('/orders', [KstTradeController::class, 'orders'])->middleware('auth:api');
	Route::get('/order/{id}', [KstTradeController::class, 'order'])->middleware('auth:api');
	Route::patch('/order/{id}', [KstTradeController::class, 'update'])->middleware('auth:api');
	Route::post('/cancel', [KstTradeController::class, 'cancel'])->middleware('auth:api');
	Route::post('/refund', [KstTradeController::class, 'refund'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'm36orders'], function(){
    Route::get('/orders', [M36OrdersController::class, 'orders'])->middleware('auth:api');
	Route::get('/order/{id}', [M36OrdersController::class, 'order'])->middleware('auth:api');
	Route::patch('/order/{id}', [M36OrdersController::class, 'update'])->middleware('auth:api');
	Route::post('/cancel', [M36OrdersController::class, 'cancel'])->middleware('auth:api');
	Route::post('/refund', [M36OrdersController::class, 'refund'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'individual'], function(){
    Route::get('/report', [IndividualController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [IndividualController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [IndividualController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [IndividualController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'individualAlb'], function(){
    Route::get('/report', [IndividualAlbController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [IndividualAlbController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [IndividualAlbController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [IndividualAlbController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'aurika'], function(){
    Route::get('/report', [AurikaController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [AurikaController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [AurikaController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [AurikaController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'entire'], function(){
    Route::get('/report', [EntireController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [EntireController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [EntireController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [EntireController::class, 'stores'])->middleware('auth:api');
	Route::get('/statuses', [EntireController::class, 'statuses'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'entire-aurika-kst'], function(){
    Route::get('/report', [EntireAurikaKstController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [EntireAurikaKstController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [EntireAurikaKstController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [EntireAurikaKstController::class, 'stores'])->middleware('auth:api');
	Route::get('/statuses', [EntireAurikaKstController::class, 'statuses'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'kstreport'], function(){
    Route::get('/report', [KstReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [KstReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [KstReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [KstReportController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'm36'], function(){
    Route::get('/report', [M36ReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [M36ReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [M36ReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [M36ReportController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'helios'], function(){
    Route::get('/report', [HeliosReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [HeliosReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [HeliosReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [HeliosReportController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'sinooil'], function(){
    Route::get('/report', [SinooilReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [SinooilReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [SinooilReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [SinooilReportController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'binom'], function(){
    Route::get('/report', [BinomReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [BinomReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [BinomReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [BinomReportController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'venoil'], function(){
    Route::get('/report', [VenoilReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [VenoilReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [VenoilReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/stores', [VenoilReportController::class, 'stores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'corp'], function(){
    Route::get('/report', [CorpReportController::class, 'report'])->middleware('auth:api');
    Route::get('/send-report', [CorpReportController::class, 'sendReport'])->middleware('auth:api');
	Route::get('/cities', [CorpReportController::class, 'cities'])->middleware('auth:api');
	Route::get('/clients', [CorpReportController::class, 'clients'])->middleware('auth:api');
	Route::get('/stores', [CorpReportController::class, 'stores'])->middleware('auth:api');
});